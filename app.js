const express = require("express");
const cors = require("cors");
const morgan = require('morgan');
require('dotenv').config()

const { connectMongo } = require('./models');
const boardsRouter = require('./routes/board.routes');
const listsRouter = require('./routes/lists.routes');
const cardsRouter = require('./routes/cards.routes');
const checklistsRouter = require('./routes/checklists.routes');
const checkItemsRouter = require("./routes/checkItems.routes");
const error = require('./ErrorHandlers/error.middleware');
const app = express();

app.use(express.json());
app.use(cors())
app.use(express.urlencoded({extended : true}))

app.use(morgan("dev"));
app.use('/boards', boardsRouter);
app.use('/lists', listsRouter);
app.use('/cards', cardsRouter);
app.use('/checklists', checklistsRouter);
app.use('/checkItems', checkItemsRouter);

const PORT =  process.env.PORT || 3000;

async function startSever(){
    await connectMongo();
    app.listen(PORT, ()=>{
        console.log(`server is listening on the port `, PORT);
    })
}


startSever()

app.use(error)
