const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const error = require("../ErrorHandlers/error");
const Checklists = require("../models/checklists.model");
const Cards = require("../models/cards.model");
const { postData } = require(".");


exports.postChecklist = (req, res, next) =>
  postData(req, res, next, Checklists, Cards, "cardId", "checklists");

exports.getChecklists = asyncErrorHandler(async (req, res, next) => {
  const result = await Checklists.find({}).populate("checkItems");
  return res.status(200).send(result);
});
