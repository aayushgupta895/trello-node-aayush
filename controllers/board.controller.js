const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const error = require("../ErrorHandlers/error");
const Board = require("../models/board.model");

exports.postBoard = asyncErrorHandler(async (req, res, next) => {
  const { name } = req.query;
  if (!name || name == "" || typeof name != "string") {
    console.log("name", name);
    throw new error("validatation error", 403);
  }
  const result = await Board.create({
    name,
  });
  return res.status(200).send(result);
});

exports.getBoard = asyncErrorHandler(async (req, res, next) => {
  const result = await Board.find({}).populate('lists');
  return res.status(200).send(result);
});


