const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const error = require("../ErrorHandlers/error");

exports.storeInParent = asyncErrorHandler(
  async (req, res, next, db, id, childId, field) => {
    const result = await db.findByIdAndUpdate(
      id,
      { $push: { [field]: childId } },
      { new: true } //returns the updated value
    );
    console.log(result, "result");
    if (!result) {
      throw new error("can't find parent", 400);
    } else {
      return result;
    }
  }
);

exports.postData = asyncErrorHandler(
  async (
    req,
    res,
    next,
    childDb,
    parentDb,
    parentField,
    childField
  ) => {
    const name = req.query.name;
    const parentId = req.query[parentField];
    if (!name || name == "" || typeof name != "string") {
      throw new error("validatation error", 403);
    }
    const child = await childDb.create({
      name,
      [parentField]: parentId,
    });
    const result = await this.storeInParent(
      req,
      res,
      next,
      parentDb,
      parentId,
      child.id,
      childField
    );
    return res.status(200).send({ result, child });
  }
);



