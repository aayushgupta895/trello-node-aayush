const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const error = require("../ErrorHandlers/error");
const Lists = require("../models/lists.model");
const Board = require("../models/board.model");
const { postData } = require(".");


exports.postList = (req, res, next) =>  postData(req, res, next,  Lists, Board,"boardId", "lists");

exports.getLists = asyncErrorHandler(async (req, res, next) => {
  const result = await Lists.find({}).populate("cards");
  return res.status(200).send(result);
});
