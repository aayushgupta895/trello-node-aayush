const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const error = require("../ErrorHandlers/error");
const Checklists = require("../models/checklists.model");
const CheckItems = require("../models/checkItems.model");
const { postData } = require(".");

exports.postCheckItem = (req, res, next) =>
  postData(req, res, next, CheckItems, Checklists, "checklistId", "checkItems");

exports.getCheckItems = asyncErrorHandler(async (req, res, next) => {
  const result = await CheckItems.find({});
  return res.status(200).send(result);
});
