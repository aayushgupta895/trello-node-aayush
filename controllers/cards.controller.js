const asyncErrorHandler = require("../ErrorHandlers/asyncErrorHandler");
const Lists = require("../models/lists.model");
const Cards = require("../models/cards.model");
const { postData } = require(".");

exports.postCard = (req, res, next) =>
  postData(req, res, next, Cards, Lists, "listId", "cards");


exports.getCards = asyncErrorHandler(async (req, res, next) => {
  const result = await Cards.find({}).populate("checklists");
  return res.status(200).send(result);
});
