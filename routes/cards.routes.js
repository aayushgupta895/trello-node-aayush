const express = require("express");
const {postCard, getCards} = require('../controllers/cards.controller')

const router = express.Router();

router.post("/", postCard);
router.get("/", getCards);

module.exports = router;
