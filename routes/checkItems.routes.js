const express = require("express");
const { postCheckItem, getCheckItems } = require('../controllers/checkItems.controller');

const router = express.Router();

router.post("/", postCheckItem);
router.get("/", getCheckItems);

module.exports = router;
