const express = require("express");
const { postBoard, getBoard } = require("../controllers/board.controller");

const router = express.Router();

router.post("/", postBoard);
router.get("/", getBoard);

module.exports = router;
