const express = require("express");
const { postList, getLists } = require("../controllers/lists.controller");

const router = express.Router();

router.post("/", postList);
router.get("/", getLists);

module.exports = router;
