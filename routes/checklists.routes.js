const express = require("express");
const { postChecklist, getChecklists } = require('../controllers/checklists.controller');

const router = express.Router();

router.post("/", postChecklist);
router.get("/", getChecklists);

module.exports = router;
