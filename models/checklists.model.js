const mongoose = require("mongoose");

const checklist = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    cardId : {
      type : mongoose.Schema.Types.ObjectId,
      ref : "Cards"
    },
    checkItems : [{
      type : mongoose.Schema.Types.ObjectId,
      ref : "CheckItems"
    }]
  },
  {
    timestamps: true,
  }
);

checklist.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Checklists = mongoose.model("Checklists", checklist);

module.exports = Checklists;
