const mongoose = require("mongoose");

const cards = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    listId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Lists",
    },
    checklists: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref : "Checklists",
      },
    ],
  },
  {
    timestamps: true,
  }
);

cards.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Cards = mongoose.model("Cards", cards);

module.exports = Cards
