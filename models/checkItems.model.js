const mongoose = require("mongoose");

const checkItems = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    checklistId: {
      type: mongoose.Schema.Types.ObjectId,
      ref : 'Checklists',
      required: true,
    },
    state: {
      type : String,
      default: "incomplete"
    }
  },
  {
    timestamps: true,
  }
);

checkItems.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const CheckItems = mongoose.model("CheckItems", checkItems);

module.exports = CheckItems;
