const mongoose = require("mongoose");

const lists = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    boardId : {
      type : mongoose.Schema.Types.ObjectId,
      ref : 'Board',
      required : true
    },
    cards : [{
      type : mongoose.Schema.Types.ObjectId,
      ref : 'Cards',
      default : []
    }]
  },
  {
    timestamps: true,
  }
);

lists.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Lists = mongoose.model("Lists", lists);

module.exports = Lists;
