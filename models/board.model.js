const mongoose = require("mongoose");

const board = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique : true,
    },
    lists : [{
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'Lists' ,
      default : []
    }],

    BackgroundColor: String,
  },
  {
    timestamps: true,
  }
);

board.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Board = mongoose.model("Board", board);

module.exports = Board;
