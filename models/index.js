const mongoose = require("mongoose");
const db = require('../config/dbconfig')

mongoose.connection.once("open", () => {
  console.log(`mongodb is connected`);
});

mongoose.connection.on("error", (err) => {
  console.log("mongodb connection error", err);
});

async function connectMongo() {
  await mongoose.connect(
    db.url
  );
}

async function disconnectMongo(){
    await mongoose.disconnect();
}

module.exports = {
    connectMongo, 
    disconnectMongo,
}
