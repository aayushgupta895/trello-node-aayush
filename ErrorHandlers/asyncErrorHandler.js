module.exports = (fn) => async(req, res, next, ...args) =>{
    try {
       return await fn(req, res, next, ...args);
    } catch (error) {
       return  next(error);
    }
}