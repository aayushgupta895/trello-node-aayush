const error = require("./error");

module.exports = (err, req, res, next) => {
  if (err instanceof error) {
    console.log(err.message); 
    return res.status(err.statusCode).send({
      message: err.message,
    }); 
  }
  return res.status(400).send({ message: err.message });
};
